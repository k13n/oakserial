package org.k13n.oakserial;

import com.mongodb.*;
import org.apache.jackrabbit.oak.InitialContent;
import org.apache.jackrabbit.oak.Oak;
import org.apache.jackrabbit.oak.api.ContentRepository;
import org.apache.jackrabbit.oak.api.ContentSession;
import org.apache.jackrabbit.oak.api.Root;
import org.apache.jackrabbit.oak.plugins.commit.ConflictValidatorProvider;
import org.apache.jackrabbit.oak.plugins.commit.JcrConflictHandler;
import org.apache.jackrabbit.oak.plugins.document.DocumentMK;
import org.apache.jackrabbit.oak.plugins.document.DocumentNodeStore;
import org.apache.jackrabbit.oak.plugins.index.property.PropertyIndexEditorProvider;
import org.apache.jackrabbit.oak.plugins.index.property.PropertyIndexProvider;
import org.apache.jackrabbit.oak.spi.security.OpenSecurityProvider;

import javax.jcr.Credentials;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.SimpleCredentials;
import javax.security.auth.login.LoginException;
import java.net.UnknownHostException;
import java.util.function.Consumer;

public class ClusterNode {
    public static String DB_NAME =  "benchmark";
    public static int  ASYNC_DELAY =  100;

    private static final Credentials CREDENTIALS = new SimpleCredentials("admin", "admin".toCharArray());
    private final int id;
    private MongoClient client;
    private DocumentNodeStore nodeStore;
    private ContentRepository repository;


    public ClusterNode(int id) {
        this.id = id;
    }


    public void connect() {
        client = getMongoDb();
        DB db = client.getDB(DB_NAME);

        DocumentMK.Builder builder = new DocumentMK.Builder()
                .setMongoDB(db)
                .setAsyncDelay(ASYNC_DELAY)
                .setClusterId(id);

        nodeStore = builder.getNodeStore();

        Oak oak = new Oak(nodeStore)
                .with(new InitialContent())
                .with(new OpenSecurityProvider())
                .with(JcrConflictHandler.createJcrConflictHandler())
                .with(new ConflictValidatorProvider())
                .with(new PropertyIndexEditorProvider())
                .with(new PropertyIndexProvider());

        repository = oak.createContentRepository();
    }


    public void tearDown() {
        nodeStore.dispose();
        client.close();
        repository = null;
        nodeStore = null;
        client = null;
    }


    public synchronized ContentSession newSession() throws UnknownHostException, NoSuchWorkspaceException, LoginException {
        if (repository == null) {
            throw new IllegalStateException("not connected");
        }
        return repository.login(CREDENTIALS, "default");
    }


    public DocumentNodeStore getNodeStore() {
        return nodeStore;
    }


    public static void dropDatabase() {
        MongoClient client = getMongoDb();
        client.getDB(DB_NAME).dropDatabase();
        client.close();
    }


    private static MongoClient getMongoDb() {
        ServerAddress address = new ServerAddress("localhost", 27017);
        return new MongoClient(address);
    }


    public void transaction(Consumer<Root> consumer) {
        try (ContentSession session = newSession()) {
            Root root = session.getLatestRoot();
            consumer.accept(root);
            root.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
