package org.k13n.oakserial;


public class App {
    public static void main(String[] args) {
        ClusterNode.DB_NAME = "benchmark";
        ClusterNode.ASYNC_DELAY = 0;

        createEmptyRepository();
        ClusterNode clusterNode1 = new ClusterNode(2);
        ClusterNode clusterNode2 = new ClusterNode(3);

        clusterNode1.connect();
        clusterNode2.connect();

        // Transaction 1 inserts /parent/child
        clusterNode1.transaction(root -> {
            System.out.println("Inserting /parent/child");
            root.getTree("/parent").addChild("child");
        });

        clusterNode1.getNodeStore().runBackgroundOperations();
        clusterNode2.getNodeStore().runBackgroundOperations();

        // Transaction 2 deletes /parent
        clusterNode2.transaction(root -> {
            System.out.println("Deleting /parent");
            root.getTree("/parent").remove();
        });

        clusterNode1.tearDown();
        clusterNode2.tearDown();
    }

    private static void createEmptyRepository() {
        ClusterNode.dropDatabase();
        ClusterNode clusterNode = new ClusterNode(1);
        clusterNode.connect();
        clusterNode.transaction(root -> root.getTree("/").addChild("parent"));
        clusterNode.tearDown();
    }
}
